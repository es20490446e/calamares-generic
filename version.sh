#! /bin/bash
set -e

calamares="https://github.com/calamares/calamares"
generic="https://gitlab.com/es20490446e/calamares-generic"


mainFunction () {
	local option="${*}"

	if [[ -z "${option}" ]] || [[ "${option}" == "builderVersion" ]]; then
		builderVersion
	elif [[ "${option}" == "calamaresVersion" ]]; then
		calamaresVersion
	elif [[ "${option}" == "builderEpoch" ]]; then
		builderEpoch
	else
		echo >&2
		echo "Invalid option:" >&2
		echo "${option}" >&2
		echo >&2
		echo "Valids are:" >&2
		echo "- builderVersion" >&2
		echo "- calamaresVersion" >&2
		echo "- builderEpoch" >&2
		echo >&2
		exit 1
	fi
}


builderEpoch () {
	repoInfo epoch "${generic}"
}


builderVersion () {
	echo "$(calamaresVersion).$(builderEpoch)"
}


checkRepoInfo () {
	if [[ ! -f "/usr/bin/repoInfo" ]]; then
		echo "repoInfo isn't installed" >&2
		exit 1
	fi
}


calamaresVersion () {
	repoInfo tags "${calamares}" |
	cut --delimiter='v' --fields=2- |
	sort --unique --reverse --version-sort |
	grep --invert-match "[[:alpha:]]" |
	head -n1
}


checkRepoInfo
mainFunction "${@}"
