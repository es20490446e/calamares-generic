#! /bin/bash
set -e

here="$(realpath "$(dirname "${0}")")"
assets="${here}/assets"
build="${here}/_BUILD"
root="${here}/_ROOT"

getSource="${build}/1-getSource"
patchSource="${build}/2-patchSource"
compile="${build}/3-compile"
buildInstall="${build}/4-buildInstall"
patchInstall="${build}/5-patchInstall"

if [[ -z "${MAKEFLAGS}" ]]; then
	MAKEFLAGS="--jobs=$(nproc)"
	export MAKEFLAGS
fi


mainFunction () {
	if [[ ! -d "${root}" ]]; then
		getSource
		patchSource
		compile

		if [[ -f "${compile}/lock" ]]; then
			buildInstall
			patchInstall
			buildRoot
		fi
	fi
}


addCalamaresNoPass () {
	so cp "${assets}/calamares-nopass.sh" "usr/bin/calamares-nopass"
}


addMenuEntry () {
	so mkdir --parents "${patchInstall}.../usr/share/applications"
	so cp "${patchSource}/calamares.desktop" "${patchInstall}.../usr/share/applications"

	changeLineInFile \
		"usr/share/applications/calamares.desktop" \
		"pkexec calamares" \
		"calamares-nopass"
}


addNoPassRules () {
	so install -Dm644 \
		"${assets}/49-nopass-calamares.rules" \
		"etc/polkit-1/rules.d/49-nopass-calamares.rules"
}


buildInstall () {
	if [[ ! -d "${buildInstall}" ]]; then
		so mkdir --parents "${buildInstall}..."
		cd "${compile}"

		DESTDIR="${buildInstall}..." so cmake --install .

		so mv "${buildInstall}..." "${buildInstall}"
	fi
}


buildRoot () {
	so rm --recursive --force "${root}"
	so cp --recursive --force "${patchInstall}" "${root}..."
	so mv "${root}..." "${root}"
}


calamaresVersion () {
	"${here}/version.sh" calamaresVersion
}


changeLineInFile () {
	local file="${1}"
	local original="${2}"
	local changed="${3}"

	if ! grep --quiet "${original}" "${file}" 2> /dev/null; then
		echo >&2
		echo "${FUNCNAME[1]}:" >&2
		echo >&2
		echo "the file:" >&2
		echo "${file}" >&2
		echo >&2
		echo "doesn't have line:" >&2
		echo "${original}" >&2
		echo >&2
		exit 1
	else
		so sed --in-place --expression "s|${original}|${changed}|g" "${file}"
	fi
}


checkDependencies () {
	local missing=()

	local dependencyLists=(
		"${here}/info/dependencies.txt"
		"${here}/info/dependencies/building.txt"
		"${here}/info/dependencies/installing.txt"
	)

	for dependencyList in "${dependencyLists[@]}"; do
		if [[ -f "${dependencyList}" ]]; then
			local lines; readarray -t lines < <(awk 'NF' "${dependencyList}")

			for line in "${lines[@]}"; do
				local name; name="$(echo "${line}" | cut --delimiter='"' --fields=2)"
				local path; path="$(echo "${line}" | cut --delimiter='"' --fields=4)"
				local web; web="$(echo "${line}" | cut --delimiter='"' --fields=6)"

				if [[ -n "${web}" ]]; then
					local web="(${web})"
				fi

				if [[ ! -f "${path}" ]]; then
					missing+=("${name}  ${web}")
				fi
			done
		fi
	done

	if [[ "${#missing[@]}" -gt 0 ]]; then
		echo "Missing required software:" >&2
		echo >&2
		printf '%s\n' "${missing[@]}" >&2
		echo >&2
		echo "Get those installed first"
		echo "and run this installer again"
		exit 1
	fi
}


compile () {
	setFlags
	so mkdir --parents "${compile}"
	so rm --force "${compile}/lock"
	cd "${compile}"

	so cmake \
		-S "${patchSource}" \
		-G Ninja \
		-D CMAKE_BUILD_TYPE="Release" \
		-D CMAKE_INSTALL_PREFIX="/usr" \
		-D CMAKE_INSTALL_LIBDIR='lib' \
		-D WITH_QT6=ON \
		-D INSTALL_CONFIG=ON \
		-D BUILD_TESTING=OFF \
		-W fPIC \
		-W no-dev

	so cmake --build .
	touch "${compile}/lock"
}


fixJobQueue () {
	if ! sed --quiet '#include <QApplication>' "src/libcalamares/JobQueue.cpp"; then
		echo "${FUNCNAME[0]}: Already fixed: Remove function" >&2
		exit 1
	else
		sed --in-place '/#include <QApplication>/d' "src/libcalamares/JobQueue.cpp"
	fi
}


getSource () {
	if [[ ! -d "${getSource}" ]]; then
		so git clone \
			"https://github.com/calamares/calamares" \
			--single-branch \
			--branch "v$(calamaresVersion)"\
			--depth 1 \
			--shallow-submodules \
			"${getSource}..."
		so mv "${getSource}..." "${getSource}"
	fi
}


makeBrandingGeneric () {
	local File Files; Files=(
		"branding.desc"
		"show.qml"
	)

	for File in "${Files[@]}"; do
		so cp --force "${assets}/${File}" "src/branding/default/${File}"
	done
}


patchInstall () {
	if [[ ! -d "${patchInstall}" ]]; then
		so cp --recursive --force "${buildInstall}"  "${patchInstall}..."
		cd "${patchInstall}..."

		addNoPassRules
		addCalamaresNoPass
		addMenuEntry

		so mv "${patchInstall}..." "${patchInstall}"
	fi
}


patchSource () {
	if [[ ! -d "${patchSource}" ]]; then
		so cp --recursive --force "${getSource}"  "${patchSource}..."
		cd "${patchSource}..."

		makeBrandingGeneric
		fixJobQueue

		so mv "${patchSource}..." "${patchSource}"
	fi
}


prepareEnvironment () {
	renice --priority 10 "${$}" >/dev/null
	checkDependencies
	removeIncompleteDirs "${here}"
	removeIncompleteDirs "${build}"
}


removeIncompleteDirs () {
	local dir="${1}"

	if [[ -d "${dir}" ]]; then
		find "${dir}" \
			-mindepth 1 \
			-maxdepth 1 \
			-type d \
			-name '*...' \
			-print0 |
		xargs -0 rm --recursive --force
	fi
}


setFlags () {
	if [[ -z "${CFLAGS}" ]]; then
		export CFLAGS="-mno-direct-extern-access -mtune=generic -O2 -pipe -fno-plt -fexceptions \
		        -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security \
		        -fstack-clash-protection -fcf-protection"
		export CXXFLAGS="$CFLAGS -Wp,-D_GLIBCXX_ASSERTIONS"
		export LDFLAGS="-Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now"
		export LTOFLAGS="-fshared -fpie -flto=auto"
	else
		CFLAGS+=" -mno-direct-extern-access"
		CXXFLAGS+=" -mno-direct-extern-access"
		export CFLAGS CXXFLAGS
	fi
}


so () {
	/bin/so "-${FUNCNAME[1]}" "${@}"
}


prepareEnvironment
mainFunction
