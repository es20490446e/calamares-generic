The [Calamares](https://github.com/calamares/calamares) installer without distro specific branding or configurations (they can be provided in some other form).

![screenshot.png](https://gitlab.com/es20490446e/calamares-generic/-/raw/main/info/screenshot.png)
